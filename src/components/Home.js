import React from 'react';
import { myLists } from '../json/myListsApi';
import { restListAPI} from '../json/jsonify';
import UserPins from './UserPins';

class UserProfile  extends React.Component {
    constructor(props){
        super(props);
        this.state={
            authentication:0,
            user_id: props.userId,
            //restLists: restListAPI(myLists, false),
            restLists: null,
            more:false,
        }

        //this.loggedIn();

    }
    toggleMoreActions = () =>{
        this.setState({ more: !this.state.more })
    }

    getActions = () =>{
        var edit = <button class="fas fa-edit"/>
        var addnew = <button title="Add New List" class="fas fa-plus-square" />
        var filter = <button title="Filter Lists" class="fas fa-filter" />
        var share = <button class="fas fa-share-square"/>
        var settings = <button title="Settings" class="fas fa-cog"/>
        var more = <button title="More Actions" onClick={this.toggleMoreActions} class="fas fa-angle-double-right"/>
            //var invite = <button class="fas fa-envelope-open-text"/>
            //var invite = <input type="image" src="/res/invitation.svg"/>
        var invite = <button id="invite"><img class="softer-shadow" alt="invitation icon" src="/foodies/res/invitation_yellow.svg"/></button>
        var more_div
        if (this.state.more===true){
            more_div = <div class="more soft-shadow"> <div class="flex-column"> {invite} {share} </div></div>
            more = <button title="More Actions" onClick={this.toggleMoreActions} class="fas fa-angle-double-down"/>
        }
        var more_wrap=<div class="inline-flex">{more}{more_div}</div>
        
        if (this.state.authentication < 2){
            return ([edit, addnew, filter,settings, more_wrap])
        }
    }
    async componentDidMount() {
        const url = "/filledLists?userID="+this.state.user_id ;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({restLists:restListAPI(data,false)});
        
    }

 
    render(){

        var heart=<icon className="fas fa-heart"/>
        return(
            <div class="home">
                <div class="actions"> {this.getActions()} </div>
                <div id="pinned-row"><div class="flex-row">{heart}<UserPins lists={myLists} />{heart}</div></div>
                <div class="list-container" > {this.state.restLists}</div>
            </div>
        )
    } 

}
export default UserProfile;
