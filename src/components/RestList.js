import React from 'react';
import ReactDOM from 'react-dom'
import EditLi from './EditLi'
//import GalleryModal from './GalleryModal'

function RestList(props) {

    const {listTitle, author, restaurants, expand} = props;
    var expand_div_class = "no-display"
    var rest_div_class = "restauarants"
    var restcont_class = "rest-container soft-shadow"
    var author_h4_class,img,tags
    var modal = false
    console.log(expand)


    //var myrests
    //THis is where you would load the list from the database baesd on the title
    /*
    if (listTitle === "Second List") { 
        myrests = ["Starbucks", "Cafe", "Lost dog Cafe", "Pho 66", "Kung Fu Tea", "Blaze Pizza", "Taco Bar", "Verizon Wireless"];
    } else{
        myrests = ["Hello", "goodbye", "test", "best", "Chipotle"];
    }
    for (let rest of restaurants){
        myrests
    }
    */


        /*
    function AddEditLi(){
        console.log("lkjdslakjfds");
        myrests.push("new");
    }
    */
    function ToggleContents(event){
        var targ= event.target;
        var targ_cont = targ.closest('.rest-container');
        var targ_ul = targ.closest('.rest-container').lastChild;
        var targ_dots = targ.closest('div').lastChild;
        var targ_tags = targ.closest('.flex-row').lastChild;
        //var targ_img = targ.closest('.flex-row').firstChild;
        if (targ_ul.className.includes("no-display")){
            //TOGGLE  EXPAND
            targ_cont.className="rest-container soft-shadow";
            targ_ul.className="restaurants";
            targ_dots.className="hidden";
            targ_tags.className="no-display";
            //targ_img.style="width:4em";
        } else{
            //TOGGLE COLLAPSE
            targ_cont.className="rest-container collapsed soft-shadow";
            targ_ul.className="restaurants, no-display";
            targ_dots.className = "show-more"
            targ_tags.className="tags flex-column";
            //targ_img.style="width:4em";
        }


    }
    function mostCommonTags(){
        return("hello,goodbye,hard-coded,testing".split(','))
        }
    function RenderTags(){
        var tagarray=[]
        for (let tag of mostCommonTags()){
            tagarray.push(<span className="tag softer-shadow">#{tag}</span>);
        }
        return(<div className="tags flex-column">{tagarray}</div>);
        
    }
    function closeModal(){
        let veil=document.getElementById("modal-veil")
        let gallery=document.getElementById("gallery-modal")
        if (veil != null){
            veil.removeChild(gallery)
            veil.remove()
        }

    }
    function openGallery(event){
        console.log("hi hhellloo????")
        //let gallery=GalleryModal(restaurants)
        let veil=document.createElement("div") 
        veil.id="modal-veil"
        veil.addEventListener("click", closeModal)
        let modal=document.createElement("div") 
        modal.id="gallery-modal"
        veil.appendChild(modal)
        let gallery=document.createElement("div") 
        gallery.id="gallery"
        modal.appendChild(gallery)
        //okay yeah idk how to do this

        //event.target.parentNode.insertBefore(veil, event.target.nextSibling)
        ReactDOM.render(veil,event.target.parentNode)
        modal=true
    }

    if ( expand === false ){
        restcont_class="rest-container collapsed soft-shadow"
        rest_div_class="restaurants, no-display";
        expand_div_class="show-more"
        author_h4_class="no-display"
        //img=<img src={restaurants[0].filePath} alt="pinned restaurant" />
        //tags=RenderTags()
    } else{
        //#@img=null
        //tags=null
        restcont_class="rest-container soft-shadow"
        rest_div_class="restaurants";
    }
    if ( modal === false) {
        modal=null
    } else { 
    }


        img=<img src={restaurants[0].filePath} alt="pinned restaurant" onClick={openGallery} />
        tags=RenderTags()
    
    return (
        <div class={restcont_class}>
            <div class="flex-row">
              {img}
              {modal}
              <div>
                <h2 className=".list-title" onClick={ToggleContents}>{listTitle}</h2>
                <h4 className={author_h4_class}>owned by <a href="profile.html?u={author}">{author}</a></h4>
                 <div class={expand_div_class}>...</div>
              </div>
              {tags}
            </div>
                <ul className={rest_div_class}>
                    { restaurants.map(r => (
                    <EditLi rest={r.name} tags={r.tags} pic={r.filePath} url={r.externalURL} />)) }
                 </ul>
         </div>
    );
}
                // <li><button onClick={AddEditLi()}>+</button></li>

export default RestList;
