import React from 'react';
import SearchBar from './SearchBar';
import {Link} from 'react-router-dom';
function NavBar(props){
    function ShowAccountMenu(){
        var ad=document.getElementById("account-dropdown");
        if ( ad.className.includes("no-display") ){
            ad.className="";
        } else{
            ad.className="no-display";
        }
    }
    return(
        <div id="nav-wrapper">
            <div id="logo-circle" class="softer-shadow"><Link to="/"><img src="/foodies/logo.png" alt="logo graphic" /></Link></div>
            <nav class="soft-shadow">
                <h1 id="logo-font">Foodies</h1>
                <ul class="right-nav">
                    <li id="search-li"> <SearchBar/></li>
                    <li><button class="fas fa-plus" title="Post" ></button></li>
                    <li><button class="fas fa-heart" title="friends" ></button></li>
                    <li><button class="fas fa-list-ul" title="List" ></button></li>
                    <li><button class="fas fa-user" title="Account" onClick={ShowAccountMenu} ></button></li>
                </ul>
                <ul id="account-dropdown" class="soft-shadow no-display">
                    <li><Link to="/profile/example_username" >Profile</Link></li>
                    <li><Link to="/list" >My Lists</Link></li>
                    <li><Link to="/friends" >Friends</Link></li>
                    <li><Link to="/settings" >Settings</Link></li>
                    <li><a href="Logout.html">Logout</a></li>
                </ul>
            </nav>
        </div>
    );
}
export default NavBar;
