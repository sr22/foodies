import React from 'react';
import {jsonify} from '../json/jsonify';
import {myProfile} from '../json/myProfile';

class Settings  extends React.Component {
    constructor(props){
        super(props);
        var json=jsonify(myProfile).reply
        this.state={
            authentication:0,
            user_id: json.user.username,
            user_real_name: json.user.realname,
         //   user_picture: json.user.picture,
            user_picture: this.userDetails(props, json),
            carousel_pics: json.user.carousel,
            privacy_acc: 0,
            privacy_list: 1,
            json:json,
            error : ""
        }

        this.userDetails=this.userDetails.bind(this)
        this.userSave=this.userSave.bind(this)

    }

    userDetails(props, json){
        //fetch
        
        if (json.user.username.includes("straw")){
            //this.setState({ user_picture : "/images/profile_straw.png" });
            return "/images/profile_straw.png"; }
        else{ return json.user.picture; }
    }
    userSave(){
        this.setState({error:"...saving..."})
        if (document.getElementById('settings-new-password').value===document.getElementById('settings-confirm-password').value){
            //do things
            if (document.getElementById('upload-img').files){
                document.getElementById('profile-picture').src=URL.createObjectURL(document.getElementById('upload-img').files[0])
                
            }
            this.setState({
                user_id:document.getElementById('settings-username').value,
                user_real:document.getElementById('settings-name').value,
                error:"successfully saved!",
                privacy_list: document.getElementById('settings-priv-list').querySelector('select').value,
                privacy_acc: document.getElementById('settings-priv-acc').querySelector('select').value,
                
            });
        } else{
            this.setState({
                error: "error: Password don't match" })
        }
            //user_pic: document.getElementById('profile-picture').files
    }
    hoverPic=()=>{
            var target=document.getElementById('profile-picture')
            if (target.className.includes('darken-img')){
                target.className="user-picture soft-shadow"
            } else{
                target.className="user-picture soft-shadow darken-img"
            }

    }
    editPic =() =>{
          document.getElementById('upload-img').click();
        
    }


        
 
    render(){
        var labelName = <label>Name: </label>
        var labelUn = <label>Username: </label>
        var editName = <span class="flex-row">{labelName}<input  type="text" id="settings-name" defaultValue={this.state.user_real_name} /></span>
        var editUn = <span class="flex-row">{labelUn}<input  type="text" id="settings-username" defaultValue={this.state.user_id} /></span>
        var h1_account=<h1>Account Settings</h1>
        var h1_user=<h1>{this.state.user_id} User Settings</h1>
        var profile_pic=<div class="img-wrapper"><img id="profile-picture" class="user-picture soft-shadow" onMouseLeave={this.hoverPic} onMouseEnter={this.hoverPic} onClick={this.editPic} src={this.state.user_picture} alt="default profile fork" /><input id="upload-img" type='file' hidden/></div>
        //var editPicture = <img src={this.state.user_picture}  alt="Edit your Profile Pic"/>
        //var saveButton = <button type="button" >Save</button>
        var userSave = <button type="button" id="settings-user-save" onClick={this.userSave}>Save</button>
        var error=<span>{this.state.error}</span>
        var saveDiv=<div id="row-user-save" className="flex-row">{error}{userSave}</div>


        var oldPass=<div><label>Old Password</label><input type="password" id="settings-old-password" /> </div>
        var newPass=<div><label>New Password</label><input type="password" id="settings-new-password" /> </div>
        var confirmPass=<div><label>Confirm New Password</label><input type="password" id="settings-confirm-password" /> </div>
        var editEmail=<div><label>Change Email</label><input type="email" id="settings-email" /></div>
    
        var editPrivacy = <p>This is the edit privacy part</p>
        var labelPriv = <label>Who can see my profile?({this.state.privacy_acc})</label>
        var labelListPriv = <label>By default, who can see my Lists? <br />(You can change the setting per list) ({this.state.privacy_list})</label>
        var privacySel = <select><option value="0">Only Me</option>
                            <option value="1">Friends</option>
                            <option value="2">Anyone</option>
                        </select>
        var privacyAcc = <div id="settings-priv-acc">{labelPriv}{privacySel}</div>
        var privacyList = <div id="settings-priv-list">{labelListPriv}{privacySel}</div>
        var delAcc = <div> <label>Delete my Account!</label><button  type="button" id="delete-acc" >danger!</button></div>

        return(
            <div id="Settings">
                <div id="user-settings" className="flex-row">
                    <div id="stats-wrapper" className="flex-column">
                        <div>{profile_pic}</div>
                    </div>
                    <div  id="user-settings-col-2" className="flex-column">
                        {h1_user}
                        {editName}
                        {editUn}
                        <textarea id="settings-bio">This is my biooooooooooooooooooooooooooooooooo</textarea>
                        {saveDiv}
                    </div>
                </div>
            <div id="account-settings">
                <form>
                    {h1_account}
                    {editEmail}
                    <div id="settings-pass">
                        {oldPass}
                        {newPass}
                        {confirmPass}
                    </div>
                    {editPrivacy}
                    {privacyAcc}
                    {privacyList}
                    {delAcc}
                    {saveDiv}
                </form>
            </div>
            </div>
        )
    } 

}
export default Settings;
/*
    render(){
        //const authenitcation= this.state.authentication
        var profile_pic=<div class="img-wrapper"><img class="user-picture soft-shadow" src={this.state.user_picture} alt="default profile fork" /></div>
        var h1=<h1>{this.state.user_real_name} ({this.state.user_id})</h1>
            //var carousel = <div id="carousel"><div class="img-wrapper"><img src="/images/sample_dish3.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/><div class="img-wrapper"><img src="/images/sample_dish.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq3.png"/> <div class="img-wrapper"><img src="/images/sample_dish2.png" /></div>  <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/> <div class="img-wrapper"><img src="/images/sample_dish3.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/><div class="img-wrapper"><img src="/images/sample_dish.png" /></div> <div class="img-wrapper"><img src="/images/sample_kbbq3.png"/> <div class="img-wrapper"><img src="/images/sample_dish2.png" /></div>  <div class="img-wrapper"><img src="/images/sample_kbbq2.png"/></div>

        return(
            <div>
                <div id="user-wrapper" class="flex-row">
                    <div id="stats-wrapper" class="flex-column">
                        <div>{profile_pic}</div>
                    </div>
                    <div class="flex-column">
                        {h1}
                        <div class="stats">This user has {this.state.restLists.length} lists, {this.state.carousel_pics.length} featured pictures,  and {this.totalRestaurants()} restaurants.</div>
                        <div id="bio">This is my biooooooooooooooooooooooooooooooooo</div>
                        <div class="profile-actions flex-row">{this.getActions()}</div>
                    </div>
                </div>
                <div id="carousel" class="flex-row">{this.carousel()}</div>
            <div class="list-container" expanded={true} > {this.state.restLists}</div>
            
        </div>

*/
