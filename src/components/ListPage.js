import React from 'react';
import {jsonify, restListAPI} from '../json/jsonify';
import { myLists } from '../json/myListsApi';
//import SearchBar from '../components/SearchBar';
import RestList from '../components/RestList';
//import EditLi from '../components/EditLi';

class ListPage  extends React.Component {
    constructor(props){
        super(props);
        var json=jsonify(myLists)
        //var restLists= restListJson(myLists, false)
        this.state={
            authentication:0,
            user_id: props.userId,
            restLists:null,
            filteredLists:null,
            json:json,
            filter:"", 
            updated:true
        }

    }

    userDetails(props, json){
        //fetch
        
        if (props.userId.includes("straw")){
            //this.setState({ user_picture : "/images/profile_straw.png" });
            return "/images/profile_straw.png"; }
        else{ return json.user.picture; }
    }

    getActions = () =>{
        var addfriend = <button class="fas fa-user-plus"/>
        var edit = <button class="fas fa-edit"/>
        var msg = <button class="fas fa-comment"/>
        var share = <button class="fas fa-share-square"/>
            //var invite = <button class="fas fa-envelope-open-text"/>
            //var invite = <input type="image" src="/res/invitation.svg"/>
            var invite = <button id="invite"><img class="softer-shadow" alt="invitation icon" src="/foodies/res/invitation_yellow.svg"/></button>
        //var searchinput= <input id="searchbar-input" type="text" onInput={this.lazySearch}></input> ;
        if (this.state.authentication < 2){
            return ([addfriend, msg, share, edit, invite])
        }
    }
    filterLazy=()=>{
        this.setState({filter:document.getElementById("filterbar-input").value, updated:false})
    }
    async componentDidMount() {
        const url = "/filledLists?userID="+this.state.user_id ;
        const response = await fetch(url);
        const data = await response.json();
        console.log(data)
        let json=restListAPI(data,false)
        this.setState({restLists:data, filteredLists:json});
        
    }

    componentDidUpdate=()=>{
        //var expand=true
        console.log("mic check? 1 2 ")
        var RestLists=[]
        //if (this.state.filter!=document.getElementById("filterbar-input").value){
        if (this.state.filter === "" && this.state.restLists != null && this.state.updated===false){
            this.setState({filteredLists:restListAPI(this.state.restLists,false), updated:true})
        }
        else if (this.state.updated===false){
            console.log("mic check?")
            for (let list of this.state.restLists){
                if (list.name.includes(this.state.filter)){
                    //RestLists.push(<RestList listTitle={list.name} author="me" expand={false} restaurants={list.restaurants} />)
                    RestLists.push(list.name)
                    console.log("hi")
                }else {
                    console.log("did this happen")
                    var rests = []
                    for ( var rest of list.restaurants )  {
                        if (rest.tags.includes(this.state.filter)){
                            rests.push(rest)
                            //rests.push(<EditLi rest={rest.name} tags={rest.tags} pic={rest.filePath} url={rest.externalURL} />)

                        } else if (rest.name.includes(this.state.filter)){
                            rests.push(rest)

                        }
                    }
                    if (rests.length>0){
                        //RestLists.push(<RestList listTitle={list.title} author="me" expand={expand} restaurants={rests} />)

                        RestLists.push(<RestList listTitle={list.name} author="me" expand={true} restaurants={rests} />)
                        
                    }
                    
                    
                }
            }
            this.setState({filteredLists:RestLists, updated:true})
        }

    }
 
    render(){
        var searchinput= <input id="filterbar-input" type="text" onChange={this.filterLazy}></input> ;

        return(
            <div>
                <div class="actions"> {this.getActions()} </div>
                {searchinput}
                <div class="list-container" > {this.state.filteredLists}</div>
            </div>

        )
    } 

}
export default ListPage;
