import React from 'react';
//import {jsonify, restListJson} from '../json/jsonify';
//import {restListJson} from '../json/jsonify';
//import {myProfile} from '../json/myProfile';
import RestList from '../components/RestList';

export default class APITest extends React.Component {
    state={
        loading:true,
        user_id:1,
        rests:null
    }

    async componentDidMount() {
        const url = "/filledLists?userID="+this.state.user_id ;
        const response = await fetch(url);
        const data = await response.json();
        console.log(data);
        console.log("hello?????");
        
        var RestLists=[];
        for (let list of data){
            RestLists.push(<RestList listTitle={list.name} author="me" expand={false} restaurants={list.restaurants} />);
        }
        await console.log(RestLists)
        this.setState({rests: RestLists, loading: false});
    }
    
    render() { 
        return ( 
            <div>
                {this.state.loading ? <div> loading!!!!!</div> : <div class='list-container'>{this.state.rests}</div>}
            </div>
        )
    }
}

