# IT390 Food Project
<div align="center">
    <img src="https://gitlab.com/sr22/it390-food-friends/raw/master/public/circle-logo_100.png" width=100 alt="Foodie Logo" />
    <p>A web service and (soon to be) app for keeping lists of your favourite restaurants and viewing your friends' in order to have food hanging synergy and make g(f)ood decisions faster.
</p>
</div>

## TO DO 
- [x] Learn React
- [x] Create Lists Page
  - [x] add fontawesome and replace some text with icons
  - [x] Add account dropdown in the upper right corner 
- [ ] Create template from Lists page
- UI
    - [x] Create Logo
    - [x] Choose Name of Project
      - [x] Add Name of Project to the top left hand of hte page
    - [x] Develop aesthetics
        - [ ] continue perpetuating aesthetic. Round corners?
        - [ ] Add more shadows?
        - [ ] turn page or at least list into a card-based view?
- [x] create login page (guilcon, external)
  - [ ] Merge to here
- [x] create registration page (guilcon, external)
  - [ ] Merge to here
- Create the rest of the pages
  - [ ] Profile
  - [ ] Settings
  - [ ] Homepage
  - [ ] Solo list page
  - [ ] solo restaurant page 
    - [ ] Invitation
- BACKEND
  - [ ] ERD
    - [x] next draft https://gitlab.com/sr22/foodies/snippets/1907267
  - [ ] Create DB
  - [ ] Spring
    - [ ]


## Screenshots
![alt text]( https://gitlab.com/sr22/it390-food-friends/raw/master/public/Screenshot_update.png "Newest Screenshot" )
![alt text]( https://gitlab.com/sr22/it390-food-friends/raw/master/public/Screenshot1.png "Screenshot 1" )



![alt text]( https://gitlab.com/sr22/it390-food-friends/raw/master/public/Screenshot2.png "Screenshot 2" )




### User Stories
  > As a foodie I use this app because me and my girls can never decide where we want to go eat even though we all have places we like

  > As a person who loves to travel I use this app to organize the places I love in the different cities I vist so that no matter how far I go I know what restaurants will feel like home. 

### Project name ideas:
  - foodies
  - foodies & their friends
 <del> - friends r 4 food
  - food iz 4 friends
  - eat in the middle
  - food contact list
  - eat w friends
  - hang or die
  - fav food fav friends
  - my fav friends eat my fav foods aka mffemff ;)</del>




This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
